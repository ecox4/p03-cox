//
//  GameView.m
//  Doodle2
//
//  Created by Patrick Madden on 2/4/17.
//  Copyright © 2017 Binghamton CSD. All rights reserved.
//

#import "GameView.h"
#include<time.h>
#include<stdlib.h>

@implementation GameView
@synthesize jumper, bricks;
@synthesize tilt;
@synthesize numJumps;
@synthesize modifiedSize,modifiedGrav;
@synthesize levels;

-(void)changeLabel{
    UILabel *label = (UILabel *)[self viewWithTag:1];
    label.text = [NSString stringWithFormat:@"Level %d",levels];
}
-(void)changeGravity{
    if(!modifiedGrav){
        if((numJumps+1)%50 == 0) {
            [jumper setDy:[jumper dy]*0.25];
        }else{
            [jumper setDy:[jumper dy]*0.75];
        }
        modifiedGrav = true;
    }else{
        [jumper setDy:6];
        modifiedGrav = false;
    }
}
-(void)changeBricks{
    for (Brick *brick in bricks)
    {
        CGFloat initWidth = CGRectGetWidth([brick frame]);
        CGFloat finalWidth;
        if(!modifiedSize){
            finalWidth = initWidth * 0.25;
            modifiedSize = true;
        }else{
            finalWidth = initWidth / 0.25;
            modifiedSize = false;
        }
        CGRectInset([brick frame], initWidth, finalWidth);
    }
}
-(void)changeBG{
    //will occur when numJumps exceeds 125,250,500, and finally at 9001
    if(numJumps == 125){
        [self setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"125.jpg"]]];
    }else if(numJumps == 250){
        [self setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"250.png"]]];
    }else if(numJumps == 500){
        [self setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"500.png"]]];
    }else if(numJumps == 9001){
        NSLog(@"It's over 9000!!!");
        [self setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"9000.png"]]];
    }
}

-(id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        CGRect bounds = [self bounds];
        
        jumper = [[Jumper alloc] initWithFrame:CGRectMake(bounds.size.width/2, bounds.size.height - 20, 20, 20)];
        [jumper setBackgroundColor:[UIColor redColor]];
        [jumper setDx:0];
        [jumper setDy:10];
        [self addSubview:jumper];
        [self makeBricks:nil];
        modifiedGrav = false;
        modifiedSize = false;
        [self setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"1.png"]]];
        [self changeLabel];
    }
    return self;
}

-(IBAction)makeBricks:(id)sender
{
    CGRect bounds = [self bounds];
    float width = bounds.size.width * .2;
    float height = 20;
    
    if (bricks)
    {
        for (Brick *brick in bricks)
        {
            [brick removeFromSuperview];
        }
    }
        
    bricks = [[NSMutableArray alloc] init];
    for (int i = 0; i < 10; ++i)
        {
            Brick *b = [[Brick alloc] initWithFrame:CGRectMake(0, 0, width, height)];
            [b setBackgroundColor:[UIColor blueColor]];
            [self addSubview:b];
            [b setCenter:CGPointMake(rand() % (int)(bounds.size.width * .8), rand() % (int)(bounds.size.height * .8))];
            [b changeBG];
            [bricks addObject:b];
        }
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
-(void)moveBricksDown{
    CGRect bounds = [self bounds];
    for (Brick *brick in bricks){
        CGRect modFrame = brick.frame;
        modFrame.origin.y += 100;
        if(modFrame.origin.y > (bounds.size.height * .7)){
            modFrame.origin.y = rand() % (int)(bounds.size.height * .25);
            modFrame.origin.x = rand() % (int)(bounds.size.width * .8);
        }
        brick.frame = modFrame;
    }
}

-(void)arrange:(CADisplayLink *)sender
{
    CFTimeInterval ts = [sender timestamp];
    
    CGRect bounds = [self bounds];
    
    // Apply gravity to the acceleration of the jumper
    [jumper setDy:[jumper dy] - .3];
    
    // Apply the tilt.  Limit maximum tilt to + or - 5
    [jumper setDx:[jumper dx] + tilt];
    if ([jumper dx] > 5)
        [jumper setDx:5];
    if ([jumper dx] < -5)
        [jumper setDx:-5];
    
    // Jumper moves in the direction of gravity
    CGPoint p = [jumper center];
    p.x += [jumper dx];
    p.y -= [jumper dy];
    
    // If the jumper has fallen below the bottom of the screen,
    // add a positive velocity to move him
    if (p.y > bounds.size.height)
    {
        [jumper setDy:10];
        p.y = bounds.size.height;
    }
    
    // If we've gone past the top of the screen, wrap around
    if (p.y < 0){
        p.y += bounds.size.height;
        [self moveBricksDown];
        ++levels;
        [self changeLabel];
    }
    // If we have gone too far left, or too far right, wrap around
    if (p.x < 0)
        p.x += bounds.size.width;
    if (p.x > bounds.size.width)
        p.x -= bounds.size.width;
    
    // If we are moving down, and we touch a brick, we get
    // a jump to push us up.
    if ([jumper dy] < 0)
    {
        for (Brick *brick in bricks)
        {
            CGRect b = [brick frame];
            if (CGRectContainsPoint(b, p))
            {
                // Yay!  Bounce!
                NSLog(@"Bounce!");
                [jumper setDy:10];
                numJumps++;
                
            }
        }
    }
    srand((unsigned)time(NULL));
    int r = rand() % 11;
    if(r > 9){
        [self changeGravity];
    }
    if(r > 3){
        [self changeBricks];
    }
    [self changeBG];
    [jumper setCenter:p];
    // NSLog(@"Timestamp %f", ts);
}

@end
